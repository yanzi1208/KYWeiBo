//
//  KYStatusPictureView.swift
//  01-我的微博
//
//  Created by apple on 15/8/4.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit
import SDWebImage

private let KYStatusPictureCellID = "KYStatusPictureCellID"

class KYStatusPictureView: UICollectionView {
    
    var status: KYStatus? {
        
        didSet{
            sizeToFit()
            reloadData()
        }
    }
    
    override func sizeThatFits(size: CGSize) -> CGSize {
        return calcViewSize()
    }
    
    
    /// 计算视图大小
    private func calcViewSize() -> CGSize{
        
        let itemSize = CGSize(width: 90, height: 90)
        
        let margin: CGFloat = 10
        
        let rowCount = 3
        pictureLayout.itemSize = itemSize
        
        
        let count = status?.pic_urls?.count ?? 0

        if count == 0 {
        
            return CGSizeZero
        }
        if count == 1 {
            
            let key = status!.pictureURLs![0].absoluteString
            
            let image = SDWebImageManager.sharedManager().imageCache.imageFromDiskCacheForKey(key)
            var size = CGSize(width: 150, height: 120)
           
            if image != nil {
            
            size = image.size
            }
            size.width = size.width < 40 ? 40 : size.width
            size.width = size.width > UIScreen.mainScreen().bounds.width ? 150 : size.width
            
            pictureLayout.itemSize = size
            return size
        }
        
        if count == 4 {
        
            let w = itemSize.width * 2 + margin
            return CGSize(width: w, height: w)
        }
        
        let row = (count - 1)/rowCount + 1
        
        let h = CGFloat(row) * itemSize.height + CGFloat(row - 1)*margin
        let w = CGFloat(rowCount) * itemSize.width + CGFloat(rowCount - 1) * margin
        return CGSize(width: w, height: h)
    }
    
    
    /// 构造方法
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: pictureLayout)
        
        backgroundColor = UIColor.lightGrayColor()
        
        registerClass( KYStatusPictureViewCell.self, forCellWithReuseIdentifier: KYStatusPictureCellID)
        
        self.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /// 懒加载
    private var pictureLayout: UICollectionViewFlowLayout = {
        
        let flowLayout = UICollectionViewFlowLayout()
        
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 10
        return flowLayout
        }()
}

extension KYStatusPictureView: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//       print(status?.pictureURL?.count)
        return status?.pictureURLs?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(KYStatusPictureCellID, forIndexPath: indexPath) as! KYStatusPictureViewCell
        
        cell.imageURL = status!.pictureURLs![indexPath.item]
        return cell
    }
 
}

/// 自定义 cell
class KYStatusPictureViewCell: UICollectionViewCell {
    
    var imageURL: NSURL? {
        didSet{
            iconView.sd_setImageWithURL(imageURL!)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        contentView.backgroundColor = UIColor.redColor()
        contentView.addSubview(iconView)
        iconView.ff_Fill(contentView)
    }
    
    private lazy var iconView: UIImageView = UIImageView()
}
