//
//  KYStatusNomalCell.swift
//  01-我的微博
//
//  Created by apple on 15/8/6.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

class KYStatusNomalCell: KYStatusCell {


    override func setupUI() {
        super.setupUI()
        // 配图
        let cons = pictureView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: contentLabel, size: CGSize(width: 290, height: 290), offset: CGPoint(x: 0, y: KYStatusCellControlMargin))
        pictureWidthCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Width)
        pictureHeightCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Height)
        pictureTopCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Top)
    }

}
