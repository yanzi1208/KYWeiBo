//
//  KYStatusCell.swift
//  01-我的微博
//
//  Created by apple on 15/8/2.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

let KYStatusCellControlMargin: CGFloat = 8.0

enum KYStatusCellIdentifier: String {

    case NormalCell = "NormalCell"
    case ForwardCell = "ForwardCell"
    
    static func cellID(status: KYStatus) -> String {
    
    return status.retweeted_status == nil ? KYStatusCellIdentifier.NormalCell.rawValue : KYStatusCellIdentifier.ForwardCell.rawValue
    
    }
}



/// 微博 cell
class KYStatusCell: UITableViewCell {
    
    var status: KYStatus? {
        
        didSet{
            topView.status = status
            pictureView.status = status
            contentLabel.text = status?.text
            pictureWidthCons?.constant = pictureView.bounds.size.width
            pictureHeightCons?.constant = pictureView.bounds.size.height
            pictureTopCons?.constant = (pictureView.bounds.size.height == 0) ? 0 : KYStatusCellControlMargin
        }
    }
    
    var pictureWidthCons: NSLayoutConstraint?
    var pictureHeightCons: NSLayoutConstraint?
    var pictureTopCons: NSLayoutConstraint?
    
    func rowHeight(status: KYStatus) -> CGFloat {
    
            self.status = status
        layoutIfNeeded()
        return CGRectGetMaxY(bottomView.frame)
    
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
            setupUI()
        }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /// 添加内部空间
    func setupUI() {
        
        contentView.addSubview(topView)
        contentView.addSubview(contentLabel)
        contentView.addSubview(pictureView)
        contentView.addSubview(bottomView)
    
        // 顶部的视图
        topView.ff_AlignInner(type: ff_AlignType.TopLeft, referView: contentView, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: 53))
        // 微博文本
        contentLabel.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: topView, size: nil, offset: CGPoint(x: KYStatusCellControlMargin, y: KYStatusCellControlMargin))
        contentView.addConstraint(NSLayoutConstraint(item: contentLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: contentView, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: -KYStatusCellControlMargin * 2))
        
        // 底部
        bottomView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: pictureView, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: 44.0), offset: CGPoint(x: -KYStatusCellControlMargin, y: KYStatusCellControlMargin))
    }
    
    //MARK: - 懒加载
    ///顶部视图
    private lazy var topView : KYStatusTopView = KYStatusTopView()
    /// 中间微博文本
    lazy var contentLabel: UILabel = {
        
        let label = UILabel(color: UIColor.darkGrayColor(), fontSize: 15)
        label.numberOfLines = 0
        label.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - KYStatusCellControlMargin*2
        return label
        }()
    /// 配图视图
    lazy var pictureView :KYStatusPictureView = KYStatusPictureView()
    
    /// 底部视图
    lazy var bottomView: KYStatusBottomView = KYStatusBottomView()
}
