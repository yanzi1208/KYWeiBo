//
//  KYStatusForwardCell.swift
//  01-我的微博
//
//  Created by apple on 15/8/6.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

class KYStatusForwardCell: KYStatusCell {
    
     override var status: KYStatus? {
        
        didSet{
            
            let name = status?.retweeted_status?.user?.name ?? ""
            let text = status?.retweeted_status?.text ?? ""
            
            forwardLabel.text = "@" + name + ":" + text
        }
    }
    
    override func setupUI() {
        super.setupUI()
        
        /// 将 backButton 添加到 pictureView 下面
        contentView.insertSubview(backButton, belowSubview: pictureView)
        contentView.insertSubview(forwardLabel, aboveSubview: backButton)
        
        backButton.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView:contentLabel, size: nil, offset: CGPoint(x: -KYStatusCellControlMargin, y: KYStatusCellControlMargin))
        backButton.ff_AlignVertical(type: ff_AlignType.TopRight, referView: bottomView, size: nil)

        
        forwardLabel.ff_AlignInner(type: ff_AlignType.TopLeft, referView: backButton, size: nil, offset: CGPoint(x: KYStatusCellControlMargin, y: KYStatusCellControlMargin))
        
        // 配图
        let cons = pictureView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: forwardLabel, size: CGSize(width: 290, height: 290), offset: CGPoint(x: 0, y: KYStatusCellControlMargin))
 
        pictureWidthCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Width)
        pictureHeightCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Height)
        pictureTopCons = pictureView.ff_Constraint(cons, attribute: NSLayoutAttribute.Top)
    }
    
    private lazy var backButton: UIButton = {
        
        let button = UIButton()
        
        button.backgroundColor = UIColor(white: 0.9, alpha: 1)
        return button
        }()
    
    private lazy var forwardLabel: UILabel = {
        
        let label = UILabel(color: UIColor.darkGrayColor(), fontSize: 14)
        label.numberOfLines = 0
        label.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - KYStatusCellControlMargin*2
        return label
        }()
    
}
