//
//  KYStatusBottomView.swift
//  01-我的微博
//
//  Created by apple on 15/8/3.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

class KYStatusBottomView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// 设置控件位置
    private func setupUI(){
        
        backgroundColor = UIColor(white: 0.95, alpha: 1)
        addSubview(forwardButton)
        addSubview(commentButton)
        addSubview(likeButton)
        
        self.ff_HorizontalTile([forwardButton, commentButton, likeButton], insets: UIEdgeInsetsZero)
    
    }
    
    /// 转发按钮
    private lazy var forwardButton: UIButton = UIButton(title: " 转发", imageName: "timeline_icon_retweet")
    /// 评论按钮
    private lazy var commentButton: UIButton = UIButton(title: " 评论", imageName: "timeline_icon_comment")
    /// 点赞按钮
    private lazy var likeButton: UIButton = UIButton(title: " 赞", imageName: "timeline_icon_unlike")

}
