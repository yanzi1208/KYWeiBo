//
//  KYStatusTopView.swift
//  01-我的微博
//
//  Created by apple on 15/8/3.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

class KYStatusTopView: UIView {

    var status: KYStatus? {
        didSet{
            if let url = status?.user?.imageURL{
            iconView.sd_setImageWithURL(url)
            }
            
            nameLabel.text = status?.user?.name ?? ""
            timeLabel.text = "刚刚"
             sourceLabel.text = "来自 土豪金"
            vipIconView.image = status?.user?.vipImage
            memberIconView.image = status?.user?.memberImage
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
    
        /// 顶部分割视图
        let sepView = UIView()
        sepView.backgroundColor = UIColor(white: 0.8, alpha: 1.0)
        addSubview(sepView)
        sepView.ff_AlignInner(type: ff_AlignType.TopLeft, referView: self, size: CGSize(width: UIScreen.mainScreen().bounds.width, height: 8))
        
        
        addSubview(iconView)
        addSubview(nameLabel)
        addSubview(timeLabel)
        addSubview(sourceLabel)
        addSubview(memberIconView)
        addSubview(vipIconView)
    
        iconView.ff_AlignVertical(type: ff_AlignType.BottomLeft, referView: sepView, size: CGSize(width: 35, height: 35), offset: CGPoint(x: 8, y: 8))
        nameLabel.ff_AlignHorizontal(type: ff_AlignType.TopRight, referView: iconView, size: nil, offset: CGPoint(x: 12, y: 0))
        timeLabel.ff_AlignHorizontal(type: ff_AlignType.BottomRight, referView: iconView, size: nil, offset: CGPoint(x: 12, y: 0))
        sourceLabel.ff_AlignVertical(type: ff_AlignType.CenterRight, referView: timeLabel, size: nil, offset: CGPoint(x: 12, y: 0))
        memberIconView.ff_AlignVertical(type: ff_AlignType.CenterRight, referView: nameLabel, size: nil, offset: CGPoint(x: 12, y: 0))
        vipIconView.ff_AlignInner(type: ff_AlignType.BottomRight, referView: iconView, size: nil, offset: CGPoint(x: 8, y: 8))
    }
    
    
    
    // MARK: - 懒加载
    /// 头像
    private lazy var iconView: UIImageView = UIImageView()
    /// 名字
    private lazy var nameLabel: UILabel = UILabel(color: UIColor.darkGrayColor(), fontSize: 14)
    /// 发表时间
    private lazy var timeLabel: UILabel = UILabel(color: UIColor.orangeColor(), fontSize:9)
    /// 来源
    private lazy var sourceLabel: UILabel = UILabel(color: UIColor.lightGrayColor(), fontSize: 9)
    /// 会员图标
    private lazy var memberIconView: UIImageView = UIImageView(image: UIImage(named: "common_icon_membership_level1"))
    /// 达人图标
    private lazy var vipIconView: UIImageView = UIImageView(image: UIImage(named: "avatar_vip"))
}
