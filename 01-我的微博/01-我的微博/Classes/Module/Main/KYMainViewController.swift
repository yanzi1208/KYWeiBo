//
//  KYMainViewController.swift
//  01-我的微博
//
//  Created by apple on 15/7/28.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

class KYMainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        addChildViewControllers()

    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupComposeButton()
    }
    
    /// 设置撰写按钮位置
    private func setupComposeButton() {
    
        let w = tabBar.bounds.width / CGFloat(childViewControllers.count)
        let rect = CGRect(x: 0, y: 0, width: w, height: tabBar.bounds.height)
    
        composedButton.frame = CGRectOffset(rect, w * 2, 0)
       
    }
    
    
    /// 撰写按钮点击
    func clickComposeButton() {
    
    
    
    print("pppp")
    
    
    }

    /// 添加所有子控制器
    private func addChildViewControllers() {
        
        addChildViewController(KYHomeTableViewController(), title: "首页", imageName: "tabbar_home")
        addChildViewController(KYMessageTableViewController(), title: "信息", imageName: "tabbar_message_center")
        addChildViewController(UIViewController())
        addChildViewController(KYDiscoverTableViewController(), title: "发现", imageName: "tabbar_discover")
        addChildViewController(KYProfileTableViewController(), title: "我", imageName: "tabbar_profile")
    }
  

    /// 添加控制器
    private func addChildViewController(vc: UITableViewController, title: String, imageName: String) {
        
        vc.title = title
        vc.tabBarItem.image = UIImage(named: imageName)
        
        let nav = UINavigationController(rootViewController: vc)
        
        addChildViewController(nav)
    }
    
    // MARK: - 懒加载
    /// 撰写按钮
    private lazy var composedButton: UIButton = {
    
    
        let btn = UIButton()
        
        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: UIControlState.Normal)
        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: UIControlState.Highlighted)
        
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
    
         self.tabBar.addSubview(btn)
        btn.addTarget(self, action: "clickComposeButton", forControlEvents: UIControlEvents.TouchUpInside)
        
        return btn
    }()
    
    
    
}
