//
//  KYVisitorLoginView.swift
//  01-我的微博
//
//  Created by apple on 15/7/28.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

protocol KYVisitorLoginViewDelegate : NSObjectProtocol{
    
    // 访客视图将要登录
    func visitorLoginViewWillLogin();
    // 访客视图将要注册
    func visitorLoginViewWillRegister();
}

/// 访客登录视图
class KYVisitorLoginView: UIView {
    
    weak var delegate: KYVisitorLoginViewDelegate?
    
    // MARK: - 设置视图信息
    /// 设置访客视图信息
    ///
    /// - parameter isHome   : 是否首页
    /// - parameter imageName: 图像名称
    /// - parameter message  : 描述文字
    
    func setupInfo(isHome isHome:Bool, imageName: String, message: String){
    
        messageLable.text = message
        iconView.image = UIImage(named: imageName)
        homeIconView.hidden = !isHome
        
        isHome ? startAnimation() : sendSubviewToBack(maskIconView)
    
    }
    
    /// 监听登录点击方法
    func clickRegisterButton(){
        delegate?.visitorLoginViewWillRegister()
    }
    
    /// 监控点击注册方法
    func clickLoginButton(){
    
    delegate?.visitorLoginViewWillLogin()
    }
    
    /// 开始动画
    private func startAnimation(){
    
        // 创建动画
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        // 旋转角度
        animation.toValue = 2 * M_PI
        // 最大圈数
        animation.repeatCount = MAXFLOAT
        // 旋转一周时间
        animation.duration = 20
        // 旋转完成后不移除
        animation.removedOnCompletion = false
        // 添加动画
        iconView.layer.addAnimation(animation, forKey: nil)
    }
    
    

    // MARK: - 设置界面
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    /// 设置界面元素
    private func setupUI(){
        backgroundColor = UIColor(white: 0.93, alpha: 1.0)
        // 添加元素
        addSubview(iconView)
        addSubview(maskIconView)
        addSubview(homeIconView)
        addSubview(messageLable)
        addSubview(registerButton)
        addSubview(loginButton)
        
        // 布局旋转的图标
        iconView.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: -40))
        // 布局房子图标
        homeIconView.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: homeIconView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: homeIconView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0))
        // 描述文字
        messageLable.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: messageLable, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: messageLable, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: messageLable, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 224))
        // 注册按钮
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: messageLable, attribute: NSLayoutAttribute.Left, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: messageLable, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 100))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 35))
        // 登录按钮
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: messageLable, attribute: NSLayoutAttribute.Right, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: messageLable, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 100))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 35))
        
        maskIconView.translatesAutoresizingMaskIntoConstraints = false
        // 此处是 addConstraints
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[subview]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["subview":maskIconView]))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[subview]-(-50)-[regButton]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["subview": maskIconView, "regButton": registerButton]))
        
        
    }
    
    // MARK: - 懒加载
    /// 旋转的小图标
    private lazy var iconView: UIImageView = {
        
        let iv = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
        return iv
        }()
    /// 遮罩视图
    private lazy var maskIconView: UIImageView = {
            
        let iv = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
        return iv
            
        }()
        
    /// 首页小房子图标
    private lazy var homeIconView: UIImageView = {
        
        let iv = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
        return iv
        }()
   
    /// 描述文字
    private lazy var messageLable: UILabel = {
    
        let lable = UILabel()
        // 字体
        lable.font = UIFont.systemFontOfSize(14)
        // 颜色
        lable.textColor = UIColor.darkGrayColor()
        // 内容
        lable.text = "关注一些人，回这里看看有什么惊喜"
        // 对齐方式
        lable.textAlignment = NSTextAlignment.Center
        // 换行
        lable.numberOfLines = 0
        // 自适应尺寸
        lable.sizeToFit()
        return lable
    }()

    /// 注册按钮
    private lazy var registerButton: UIButton = {
    
        let btn = UIButton()
        btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        btn.setTitle("注册", forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
        btn.addTarget(self, action: "clickRegisterButton", forControlEvents: UIControlEvents.TouchUpInside)
        return btn
    }()
    
    /// 登录按钮
    private lazy var loginButton: UIButton = {
    
        let btn = UIButton()
        btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        btn.setTitle("登录", forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        btn.addTarget(self, action: "clickLoginButton", forControlEvents: UIControlEvents.TouchUpInside)
        return btn
    }()
    
    
}
