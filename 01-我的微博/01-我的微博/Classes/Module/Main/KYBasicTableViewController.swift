//
//  KYBasicTableViewController.swift
//  01-我的微博
//
//  Created by apple on 15/7/28.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

class KYBasicTableViewController: UITableViewController, KYVisitorLoginViewDelegate {

    /// 用户是否登录
    var userLogon = KYUserAccountModel.userLogon
    
    /// 访客视图
    var visitorView : KYVisitorLoginView?
    
    override func loadView() {
        userLogon ? super.loadView() : setupVisitorView()
        
    }
    
    
    private func setupVisitorView() {
    
    visitorView = KYVisitorLoginView()
        visitorView?.delegate = self
        
        view = visitorView
        
        // 设置左侧为注册
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: UIBarButtonItemStyle.Plain, target: self, action: "visitorLoginViewWillRegister")
        navigationItem
        
        // 设置右侧为登录
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: UIBarButtonItemStyle.Plain, target: self, action: "visitorLoginViewWillLogin")

    }
    
    /// 点击注册按钮
    func visitorLoginViewWillRegister() {
        print("注册")
    
    
    }
    
    /// 点击登录按钮
    func visitorLoginViewWillLogin() {
        
        let nav = UINavigationController(rootViewController: KYOAuthViewController())
        presentViewController(nav, animated: true, completion: nil)
        
    }
    
    
}
