//
//  KYNewFeatureViewController.swift
//  01-我的微博
//
//  Created by apple on 15/7/31.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class KYNewFeatureViewController: UICollectionViewController {
    
    /// 图像总数
    private let imageCount = 4
    /// 创建流水布局
    private let layout = KYFlowLayout()
    
    // 因为collectionView 是指定的构造函数 所以不能写 override
    init() {
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        //        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView!.registerClass(KYNewFeatureCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    // MARK: UICollectionViewDataSource
    /// 每组有多少个
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageCount
    }
    /// cell 内容
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! KYNewFeatureCell
        
        cell.imageIndex = indexPath.item
        return cell
    }
    
    /// 完成显示cell  消失之前的 cell
    override func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        /// 显示的按钮
        let path = collectionView.indexPathsForVisibleItems().last!
        // 判断是否末尾的 indexPath
        if path.item == imageCount - 1 {
        
            // 播放动画
            let cell = collectionView.cellForItemAtIndexPath(path) as! KYNewFeatureCell
            cell.startBtnAnmi()
        }
    }
}

/// 自定义cell
class KYNewFeatureCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        prepareUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareUI()
    }
    /// 图片索引
    private var imageIndex: Int = 0 {
        didSet{
            iconView.image = UIImage(named: "new_feature_\(imageIndex + 1)")
            startButton.hidden = true
        }
    }
    
    /// 按钮点击
    func stratBtnClick() {
       NSNotificationCenter.defaultCenter().postNotificationName(KYRootViewControllerSwitchNotification, object: true)
    }
    
    /// 加载图像
    private func prepareUI() {
        
        contentView.addSubview(iconView)
        contentView.addSubview(startButton)
        
        // 自动布局
        iconView.ff_Fill(contentView)
        
        startButton.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: contentView, size: nil, offset: CGPoint(x: 0, y: -160))
   
    
    }
    
    
    /// 设置按钮动画
    private func startBtnAnmi() {
    
        startButton.hidden = false
        startButton.transform = CGAffineTransformMakeScale(0, 0)
        // 设置按钮不能交互
        self.startButton.userInteractionEnabled = false
        UIView.animateWithDuration(1.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: UIViewAnimationOptions(rawValue: 0), animations: { () -> Void in
            /// 设置动画
            /// 设置 button 为初始形变  CGAffineTransformIdentity--初始形变
            self.startButton.transform = CGAffineTransformIdentity

            }) { (_) -> Void in
                /// 动画结束的时候 回调
                self.startButton.userInteractionEnabled = true
        }
    }
    
    // MARK: - 懒加载
    /// 图片
    private lazy var iconView = UIImageView()
    
    /// 开始按钮
    private lazy var startButton: UIButton = {
        
        let button = UIButton()
        button.setBackgroundImage(UIImage(named: "new_feature_finish_button"), forState: UIControlState.Normal)
        button.setBackgroundImage(UIImage(named: "new_feature_finish_button_highlighted"), forState: UIControlState.Highlighted)
        button.setTitle("点击开始", forState: UIControlState.Normal)
        
        button.sizeToFit()
        button.addTarget(self, action: "stratBtnClick", forControlEvents: UIControlEvents.TouchUpInside)
        return button
        }()
    
}

/// 自定义流水布局
private class KYFlowLayout: UICollectionViewFlowLayout {
    
    private override func prepareLayout() {
        itemSize = collectionView!.bounds.size
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
        
        scrollDirection = UICollectionViewScrollDirection.Horizontal
        collectionView!.pagingEnabled = true
        collectionView!.showsHorizontalScrollIndicator = false
        collectionView!.bounces = false
    }
    
    
}




