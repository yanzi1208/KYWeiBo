//
//  KYWelcomeViewController.swift
//  01-我的微博
//
//  Created by apple on 15/7/31.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit
import SDWebImage

class KYWelcomeViewController: UIViewController {
    
    // 图形底部约束
    private var iconBottomCons: NSLayoutConstraint?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareUI()
        
        // 加载头像
        if let urlString = KYUserAccountModel.loadAccount()?.avatar_large {
        
            iconView.sd_setImageWithURL(NSURL(string: urlString)!)
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // 自动布局中, 修改约束不会立即生效, 只是添加一个标记, 统一由自动布局系统来更新约束
        // 使用 框架 这里注意 要改成负号
        iconBottomCons?.constant = -UIScreen.mainScreen().bounds.height - iconBottomCons!.constant
        
        UIView .animateWithDuration(1.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5.0, options: UIViewAnimationOptions(rawValue: 0), animations: { () -> Void in
            
            // 强制更新约束
                self.view.layoutIfNeeded()
            
            }) { (_) -> Void in
              
                NSNotificationCenter.defaultCenter().postNotificationName(KYRootViewControllerSwitchNotification, object: true)

        }
    }
    
    /// 准备界面
    private func prepareUI() {
        
        view.addSubview(backgImageView)
        view.addSubview(iconView)
        view.addSubview(iconLabel)
        
        // 自动布局
        // 背景
        backgImageView.ff_Fill(view)
    
        // 头像
        let cons = iconView.ff_AlignInner(type: ff_AlignType.BottomCenter, referView: view, size: CGSize(width: 90, height: 90), offset: CGPoint(x: 0, y: -160))
        iconBottomCons = iconView.ff_Constraint(cons, attribute: NSLayoutAttribute.Bottom)
        
        // 欢迎回来
        
        iconLabel.ff_AlignVertical(type: ff_AlignType.BottomCenter, referView: iconView, size: nil, offset: CGPoint(x: 0, y: 16))
 
    }
    
    // MARK: - 懒加载控件
    /// 背景图片
    private lazy var backgImageView: UIImageView = UIImageView(image: UIImage(named: "ad_background"))
    /// 头像
    private lazy var iconView: UIImageView = {
        
        let iv = UIImageView(image: UIImage(named: "avatar_default_big"))
        // 裁剪图片
        iv.layer.masksToBounds = true
        // 裁剪半径是 45
        iv.layer.cornerRadius = 45
        return iv
        }()
    /// 欢迎回来的label
    private lazy var iconLabel: UILabel = {
        
        let label = UILabel()
        label.text = "欢迎回来"
        // 按照文字自适应大小
        label.sizeToFit()
        return label
        }()
    
    
}
