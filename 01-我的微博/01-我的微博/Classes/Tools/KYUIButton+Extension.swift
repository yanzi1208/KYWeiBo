//
//  KYUIButton+Extension.swift
//  01-我的微博
//
//  Created by apple on 15/8/3.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

/// 提供 button 默认属性扩展
extension UIButton {

    convenience init(title: String, imageName: String, fontSize: CGFloat = 12, color: UIColor = UIColor.darkGrayColor()) {
    
    self.init()
        
    setTitle(title, forState: UIControlState.Normal)
        setImage(UIImage(named: imageName), forState: UIControlState.Normal)
        setTitleColor(color, forState: UIControlState.Normal)
        titleLabel?.font = UIFont.systemFontOfSize(fontSize)
    }

}
