//
//  KYUILabel+Extension.swift
//  01-我的微博
//
//  Created by apple on 15/8/3.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

/// 扩展中只能提供便利的构造函数
extension UILabel {
    convenience init(color: UIColor, fontSize: CGFloat) {
    
        self.init()
        textColor = color
        font = UIFont.systemFontOfSize(fontSize)
    }

}
