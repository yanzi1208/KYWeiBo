//
//  AppDelegate.swift
//  01-我的微博
//
//  Created by apple on 15/7/27.
//  Copyright © 2015年 KY. All rights reserved.
//

import UIKit

let KYRootViewControllerSwitchNotification = "KYRootViewControllerSwitchNotification"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        /// 注册通知
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "switchRootViewController:", name: KYRootViewControllerSwitchNotification, object: nil)
        
        print(KYUserAccountModel.loadAccount())
        // 界面启动之前修改颜色 放在后面修改容易出问题
        setupAppearance()
        
        // 设置主窗口
        window = UIWindow(frame: UIScreen .mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        window?.rootViewController = KYDefaultViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    
    /// 这里程序销毁的时候才会注销 一般会根据习惯加上
    deinit {
    
    NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func switchRootViewController(notification: NSNotification) {
    
        let isMain = notification.object as! Bool
    
        window?.rootViewController = isMain ? KYMainViewController() : KYWelcomeViewController()
    }
    
   
    /// 返回启动默认的控制器
    private func KYDefaultViewController() -> UIViewController {
    
        // 没有登录就返回主控制器
        if !KYUserAccountModel.userLogon {
        
            return KYMainViewController()
        }
    
        // 判断是否新版本 如果是返回 新特性 不是就返回 欢迎界面
        return isNewUpdate() ? KYNewFeatureViewController() : KYWelcomeViewController()
    
    }
    
    
    /// 检查是否是新版本
    private func isNewUpdate() -> Bool {
    
        // 1.获取当前版本
        let currentVersion = Double(NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String)!
        // 2.获取程序之前的版本
        let sandboxVersionKey = "sandboxVersionKey"
        let sandboxVersion = NSUserDefaults.standardUserDefaults().doubleForKey(sandboxVersionKey)
        // 3.将当前版本保存到偏好设置
        NSUserDefaults.standardUserDefaults().setDouble(currentVersion, forKey: sandboxVersionKey)
        // 4.返回比较结果
        return currentVersion > sandboxVersion
    }
    
    
    /// 设置全局外观
    private func setupAppearance(){
    
        UINavigationBar.appearance().tintColor = UIColor.orangeColor()
        UITabBar.appearance().tintColor = UIColor.orangeColor()
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

